﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPanel : MonoBehaviour {

	public GameObject[] PanelNo = new GameObject[7];		//Declaring variables and objects
	public GameObject buttonLeft;
	public GameObject buttonRight;

	private int count;	//0-4

	// Use this for initialization
	void Start () {
		count = 0;							//initializing variables
		buttonLeft.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			changePanel(1);
		}else if(Input.GetKeyDown(KeyCode.LeftArrow)){
			changePanel (-1);
		}
	}

	public void changePanel(int changeValue){
		if (count + changeValue <= 6 && count + changeValue >= 0) {		//if count is in the range o 0 to 4
			count += changeValue;		//change panel
			if (count == 0) {
				buttonLeft.SetActive (false);		//set button active
				buttonRight.SetActive (true);
			} else if (count == 6) {
				buttonLeft.SetActive (true);		//set button active
				buttonRight.SetActive (false);
			} else {
				buttonLeft.SetActive (true);		//set button active
				buttonRight.SetActive (true);
			}
			for (byte i = 0; i < 7; i++) {
				if (i != count) {
					PanelNo [i].SetActive (false);	//activate and disactivate panel
				} else {
					PanelNo [i].SetActive (true);
				}
			}
		}
	}
}
