﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour {

	public AudioClip audioClip;			//Declaring objects
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {
		playMusic ();					//calling of playMusic method
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void playMusic(){
		audioSource.clip = audioClip;	//play audio
		audioSource.Play();
	}
}
