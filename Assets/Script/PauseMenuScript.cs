﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenuScript : MonoBehaviour {

	public GameObject Menu;			//declaring object

	// Use this for initialization
	void Start () {
		
	}

	public void pause(){
		Menu.SetActive (true);		//activate menu
		Time.timeScale = 0;			//stop time
	}

	public void resume(){
		Menu.SetActive (false);		//disactivate menu
		Time.timeScale = 1;
	}
}
