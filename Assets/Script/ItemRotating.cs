﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotating : MonoBehaviour {

	public float m_TurnSpeed = 180f;		//Declaring and initializing variable 

	private Rigidbody m_Rigidbody;

	private void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();		//get component from rigidbody
	}

	// Use this for initialization
	private void Start () {
		
	}
	
	// Update is called once per frame
	private void Update () {
		Turn ();		//calling of Turn method
	}

	private void Turn(){
		float turn = m_TurnSpeed * Time.deltaTime;
		Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);		//item movement
	}
}