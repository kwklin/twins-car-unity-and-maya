﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GreenCarMovement : MonoBehaviour {

	public int m_PlayerNumber;					//Declaring variable and objects
	public float m_Speed = 6f;
	public float m_TurnSpeed = 180f;
	public GameObject opponent;
	public bool m_OpponentReverse;
	public bool m_OpponentFrozen;
	public GameObject BlueSliderImage;
	public Slider ReverseTimeSlider;
	public Text BlueReverseText;
	public float m_ReverseTime = 25f;
	public float m_FrozenTime = 15f;
	public float m_deductTime = 90f;
	public float m_HideAndSeekTime = 60f;
	public GameObject m_OpponentIceCube;
	public GameObject BlueFrozenSliderImage;
	public Slider BlueFrozenTimeSlider;
	public Text BlueFrozenText;
	//public Text GreenCoinText;						//delete this
	public Text GreenGasText;
	public BlueCarMovement _ScriptBlueCarMovement;
	public BlueIceCube _ScriptBlueIceCube;
	//public byte m_NumCoinsCollected;		//Delete this
	public byte m_NumGasCollected;
	public AudioClip Reverse;
	public AudioClip Frozen;
	public AudioClip GetCoin;
	public AudioClip DropCoin;
	public AudioSource audioSource;
	public TimeInGame _ScriptTimeInGame;
	public byte m_NumExtraGas;
	public GameObject[] ItemHideAndSeek = new GameObject[8];
	public GameObject RedHideAndSeekSliderImage;
	public Slider RedHideAndSeekSlider;
	public Text RedHideAndSeekSliderText;
	public GameObject TextBlueHide;
	public AudioSource timeticking;

	private string m_MovementAxisName;
	private string m_TurnAxisName;
	private Rigidbody m_Rigidbody;
	private float m_MovementInputValue;
	private float m_TurnInputValue;
	private float m_reverseTimeLeft;
	private float m_frozenTimeLeft;
	private GameObject tempObject;
	//private GameObject[] m_CoinsCollected;	//delete this
	private GameObject[] m_GasCollected;
	private float m_HideAndSeekTimeLeft;
	private bool m_HideAndSeekMode;

	private void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();
	}

	private void OnEnable(){
		m_Rigidbody.isKinematic = false;
		m_MovementInputValue = 0f;
		m_TurnInputValue = 0f;
	}

	// Use this for initialization
	private void Start () {
		m_MovementAxisName = "Vertical" + m_PlayerNumber;		//initializing variable
		m_TurnAxisName = "Horizontal" + m_PlayerNumber;
		m_reverseTimeLeft = 0f;
		m_frozenTimeLeft = 0f;
		m_OpponentReverse = false;
		m_OpponentFrozen = false;
		BlueSliderImage.SetActive (false);
		BlueFrozenSliderImage.SetActive (false);
		//m_CoinsCollected = new GameObject[33];	//Delete this
		m_GasCollected = new GameObject[33];
		//m_NumCoinsCollected = 0;				//Delete this
		m_NumGasCollected = 0;
		m_NumExtraGas = 3;
		m_HideAndSeekTimeLeft = 0;
		m_HideAndSeekMode = false;
	}

	// Update is called once per frame
	private void Update () {
		m_MovementInputValue = Input.GetAxis (m_MovementAxisName);		//get input axis
		m_TurnInputValue = Input.GetAxis (m_TurnAxisName);
		if (m_reverseTimeLeft > 0) {			//if reverse item is collected
			if (m_reverseTimeLeft > 60) {
				m_reverseTimeLeft = 60;
			}
			BlueSliderImage.SetActive(true);		//activate slider 
			m_reverseTimeLeft -= Time.deltaTime;	//countdown
			ReverseTimeSlider.value = m_reverseTimeLeft;		//set value of slider
			BlueReverseText.text = ("Reversed\n" + Mathf.Round(m_reverseTimeLeft));			//output value in text
			opponent.GetComponent<BlueCarMovement> ().m_OpponentReverse = true;		//set opponent's action to reverse
		} else {
			BlueSliderImage.SetActive(false);			//disactivate image
			m_reverseTimeLeft = 0;
			opponent.GetComponent<BlueCarMovement> ().m_OpponentReverse = false;
		}
		if (m_frozenTimeLeft > 0) {
			BlueFrozenSliderImage.SetActive(true);		//activate slider image
			m_frozenTimeLeft -= Time.deltaTime;			//countdown
			BlueFrozenTimeSlider.value = m_frozenTimeLeft;		//set value of slider
			BlueFrozenText.text = ("Frozen\n" + Mathf.Round (m_frozenTimeLeft));		//output value in text
			opponent.GetComponent<BlueCarMovement> ().m_OpponentFrozen = true;		//freeze opponent
			_ScriptBlueIceCube.playVideo();
		} else {
			BlueFrozenSliderImage.SetActive(false);		//disactivate slider image
			m_frozenTimeLeft = 0;
			opponent.GetComponent<BlueCarMovement> ().m_OpponentFrozen = false;		//cancel freezing
			_ScriptBlueIceCube.endVideo ();			//calling of endVideo method
		}
		if (m_HideAndSeekTimeLeft > 0) {
			m_HideAndSeekTimeLeft -= Time.deltaTime;
			RedHideAndSeekSliderImage.SetActive (true);
			RedHideAndSeekSlider.value = m_HideAndSeekTimeLeft;
			RedHideAndSeekSliderText.text = ("Hide And Seek\n" + Mathf.Round (m_HideAndSeekTimeLeft));
			TextBlueHide.SetActive (true);
		} else {
			RedHideAndSeekSliderImage.SetActive (false);
			m_HideAndSeekMode = false;
			m_HideAndSeekTimeLeft = 0;
			TextBlueHide.SetActive (false);
		}
	}

	private void FixedUpdate(){
		if (m_OpponentFrozen == false) {
			Move ();			//calling of Move and Turn method
			Turn ();
		}
	}

	private void Move(){
		Vector3 movement;
		if (m_OpponentReverse == false) {
			movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;		//usual movement
		} else {
			movement = transform.forward * -m_MovementInputValue * m_Speed * Time.deltaTime;	//reversed movement
		}
		m_Rigidbody.MovePosition (m_Rigidbody.position + movement);
	}

	private void Turn(){
		float turn;
		if (m_OpponentReverse == false) {
			turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;			//usual movement
		} else {
			turn = -m_TurnInputValue * m_TurnSpeed * Time.deltaTime;		//reversed movement
		}
		Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "ItemReverseAction") {
			other.gameObject.SetActive (false);			//disactivate item
			m_reverseTimeLeft += m_ReverseTime;			//add reverse time
			tempObject = other.gameObject;
			audioSource.clip = Reverse;					//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitTwentyFiveSeconds");
		} else if (other.gameObject.tag == "ItemFrozen") {
			other.gameObject.SetActive (false);			//disactivate item
			m_frozenTimeLeft += m_FrozenTime;			//add frozen time
			tempObject = other.gameObject;
			audioSource.clip = Frozen;					//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitFifteenSeconds");
		} else if (other.gameObject.tag == "Gas") {
			//m_CoinsCollected [m_NumCoinsCollected] = other.gameObject;		//delete this
			m_GasCollected [m_NumGasCollected] = other.gameObject;
			other.gameObject.SetActive (false);			//disactivate coin
			//m_NumCoinsCollected++;			//add one to coins collected		delete this
			m_NumGasCollected++;
			audioSource.clip = GetCoin;		//play sound effect
			audioSource.Play ();
			//GreenCoinText.text = ("Coins: " + m_NumCoinsCollected);			//delete this
			GreenGasText.text = ("Gas: " + (m_NumGasCollected + m_NumExtraGas));
		} else if (other.gameObject.tag == "ItemDeductGas") {
			_ScriptBlueCarMovement.deductCoins ();
			other.gameObject.SetActive (false);			//disactivate item
			tempObject = other.gameObject;
			audioSource.clip = DropCoin;			//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitNinetySeconds");
		} else if (other.gameObject.tag == "ItemHideAndSeek") {
			for (byte i = 0; i < 8; i++) {
				ItemHideAndSeek [i].SetActive (false);
			}
			m_HideAndSeekTimeLeft += 60f;
			m_HideAndSeekMode = true;
			timeticking.Play ();
			StartCoroutine ("waitTenSeconds");
		} else if (other.gameObject == opponent) {
			if (m_HideAndSeekMode == true) {
				TextBlueHide.SetActive (false);
				opponent.GetComponent<BlueCarMovement> ().textGreenHide.SetActive (false);
				timeticking.Pause ();
				_ScriptTimeInGame.m_HideAndSeekMode= true;
				_ScriptTimeInGame.GreenOrBlueWins = 0;
				_ScriptTimeInGame.wholeGameCountingDownTime = -1;
			}
		}
	}

	IEnumerator waitTwentyFiveSeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (m_ReverseTime + 2f);
		m_LocalTemp.SetActive (true);			//activate item after 27s
	}

	IEnumerator waitFifteenSeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (30f);
		m_LocalTemp.SetActive (true);			//activate item after 30s
	}

	IEnumerator waitNinetySeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (60);
		m_LocalTemp.SetActive (true);			//activate item after 60s
	}

	IEnumerator waitTenSeconds(){
		yield return new WaitForSeconds (60);	//activate item after 10s
		for (byte i = 0; i < 8; i++) {
			ItemHideAndSeek [i].SetActive (true);
		}
	}

	public void deductCoins(){

		/*
		if (m_NumCoinsCollected > 0) {
			m_CoinsCollected [0].SetActive (true);		//activate the first coin collected
			m_NumCoinsCollected--;
			for (byte i = 0; i < m_NumCoinsCollected; i++) {		//switch value
				m_CoinsCollected [i] = m_CoinsCollected [i + 1];
			}
			GreenCoinText.text = ("Coins: " + m_NumCoinsCollected);		//output value
		}*/

		if (m_NumGasCollected > 0) {
			m_GasCollected [0].SetActive (true);
			m_NumGasCollected--;
			for (byte i = 0; i < m_NumGasCollected; i++) {
				m_GasCollected [i] = m_GasCollected [i + 1];
			}
			GreenGasText.text = ("Gas: " + (m_NumGasCollected + m_NumExtraGas));
		} else {
			if (m_NumExtraGas > 1) {
				m_NumExtraGas--;
				GreenGasText.text = "Gas: " + m_NumExtraGas;
			} else {
				TextBlueHide.SetActive (false);
				opponent.GetComponent<BlueCarMovement> ().textGreenHide.SetActive (false);
				opponent.GetComponent<BlueCarMovement> ().timeticking.Pause ();
				m_NumExtraGas--;		//also output 0
				GreenGasText.text = "Gas: " + m_NumExtraGas;
				_ScriptTimeInGame.wholeGameCountingDownTime = -1;
			}
		}
	}
}
