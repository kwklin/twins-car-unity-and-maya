﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlueCarMovement : MonoBehaviour {

	public int m_PlayerNumber;							//Declaring variable
	public float m_Speed = 6f;
	public float m_TurnSpeed = 180f;
	public GameObject opponent;
	public bool m_OpponentReverse;
	public bool m_OpponentFrozen;
	public GameObject GreenSliderImage;
	public Slider ReverseTimeSlider;
	public Text GreenReverseText;
	public float m_ReverseTime = 25f;
	public float m_FrozenTime = 15f;
	public float m_deductTime = 90f;
	public float m_HideAndSeekTime = 60f;
	public GameObject m_OpponentIceCube;
	public GameObject GreenFrozenSliderImage;
	public Slider GreenFrozenTimeSlider;
	public Text GreenFrozenText;
	//public Text BlueCoinText;
	public Text BlueGasText;
	public GreenCarMovement _ScriptGreenCarMovement;
	public GreenIceCube _ScriptGreenIceCube;
	//public byte m_NumCoinsCollected;
	public byte m_NumGasCollected;
	public AudioClip Reverse;
	public AudioClip Frozen;
	public AudioClip GetCoin;
	public AudioClip DropCoin;
	public AudioSource audioSource;
	public TimeInGame _ScriptTimeInGame;
	public byte m_NumExtraGas;
	public GameObject[] ItemHideAndSeek = new GameObject[8];
	public GameObject RedHideAndSeekSliderImage;
	public Slider RedHideAndSeekSlider;
	public Text RedHideAndSeekSliderText;
	public GameObject textGreenHide;
	public AudioSource timeticking;

	private string m_MovementAxisName;
	private string m_TurnAxisName;
	private Rigidbody m_Rigidbody;
	private float m_MovementInputValue;
	private float m_TurnInputValue;
	private float m_reverseTimeLeft;
	private float m_frozenTimeLeft;
	private GameObject tempObject;
	//private GameObject[] m_CoinsCollected;
	private GameObject[] m_GasCollected;
	private float m_HideAndSeekTimeLeft;
	private bool m_HideAndSeekMode;

	private void Awake(){
		m_Rigidbody = GetComponent<Rigidbody> ();			//get rigidbody component
	}

	private void OnEnable(){
		m_Rigidbody.isKinematic = false;			//initialize values
		m_MovementInputValue = 0f;
		m_TurnInputValue = 0f;
	}

	// Use this for initialization
	private void Start () {
		m_MovementAxisName = "Vertical" + m_PlayerNumber;		//initialize value;
		m_TurnAxisName = "Horizontal" + m_PlayerNumber;
		m_reverseTimeLeft = 0f;
		m_frozenTimeLeft = 0f;
		m_OpponentReverse = false;
		m_OpponentFrozen = false;
		GreenSliderImage.SetActive (false);
		GreenFrozenSliderImage.SetActive (false);
		//m_CoinsCollected = new GameObject[33];
		m_GasCollected = new GameObject[33];
		//m_NumCoinsCollected = 0;
		m_NumGasCollected = 0;
		m_NumExtraGas = 3;
		m_HideAndSeekTimeLeft = 0;
		m_HideAndSeekMode = false;
	}

	// Update is called once per frame
	private void Update () {
		m_MovementInputValue = Input.GetAxis (m_MovementAxisName);			//Get horizontal and vertical axis
		m_TurnInputValue = Input.GetAxis (m_TurnAxisName);
		if (m_reverseTimeLeft > 0) {		//reverse is activated
			if (m_reverseTimeLeft > 60) {
				m_reverseTimeLeft = 60;
			}
			GreenSliderImage.SetActive(true);			//set slider active
			m_reverseTimeLeft -= Time.deltaTime;	//countdown
			ReverseTimeSlider.value = m_reverseTimeLeft;		//output value on slider
			GreenReverseText.text = ("Reversed\n" + Mathf.Round(m_reverseTimeLeft));		//output value on text
			opponent.GetComponent<GreenCarMovement> ().m_OpponentReverse = true;			//set opponent's reverse
		} else {
			GreenSliderImage.SetActive(false);
			m_reverseTimeLeft = 0;
			opponent.GetComponent<GreenCarMovement> ().m_OpponentReverse = false;
		}
		if (m_frozenTimeLeft > 0) {
			GreenFrozenSliderImage.SetActive(true);		//if frozen is activated
			m_frozenTimeLeft -= Time.deltaTime;			//countdown
			GreenFrozenTimeSlider.value = m_frozenTimeLeft;		//output value on slider
			GreenFrozenText.text = ("Frozen\n" + Mathf.Round (m_frozenTimeLeft));		//output value on text
			opponent.GetComponent<GreenCarMovement> ().m_OpponentFrozen = true;
			_ScriptGreenIceCube.playVideo();			//start animation
		} else {
			GreenFrozenSliderImage.SetActive(false);
			m_frozenTimeLeft = 0;
			opponent.GetComponent<GreenCarMovement> ().m_OpponentFrozen = false;
			_ScriptGreenIceCube.endVideo ();
		}
		if (m_HideAndSeekTimeLeft > 0) {
			m_HideAndSeekTimeLeft -= Time.deltaTime;
			RedHideAndSeekSliderImage.SetActive (true);
			RedHideAndSeekSlider.value = m_HideAndSeekTimeLeft;
			RedHideAndSeekSliderText.text = ("Hide And Seek\n" + Mathf.Round (m_HideAndSeekTimeLeft));
			textGreenHide.SetActive (true);
		} else {
			RedHideAndSeekSliderImage.SetActive (false);
			m_HideAndSeekMode = false;
			m_HideAndSeekTimeLeft = 0;
			textGreenHide.SetActive (false);
		}
	}

	private void FixedUpdate(){
		if (m_OpponentFrozen == false) {		//if it's not frozen
			Move ();			//Calling of Move and Turn method
			Turn ();
		}
	}

	private void Move(){
		Vector3 movement;
		if (m_OpponentReverse == false) {		//if reverse is not activated
			movement = transform.forward * m_MovementInputValue * m_Speed * Time.deltaTime;
		} else {				//if reverse is activated
			movement = transform.forward * -m_MovementInputValue * m_Speed * Time.deltaTime;		//go in opposite direction
		}
		m_Rigidbody.MovePosition (m_Rigidbody.position + movement);
	}

	private void Turn(){
		float turn;
		if (m_OpponentReverse == false) {		//if reverse is not activated
			turn = m_TurnInputValue * m_TurnSpeed * Time.deltaTime;
		} else {				//if reverse is activated
			turn = -m_TurnInputValue * m_TurnSpeed * Time.deltaTime;		//turn in opposite direction
		}
		Quaternion turnRotation = Quaternion.Euler (0f, turn, 0f);
		m_Rigidbody.MoveRotation (m_Rigidbody.rotation * turnRotation);
	}

	void OnTriggerEnter(Collider other){
		if (other.gameObject.tag == "ItemReverseAction") {
			other.gameObject.SetActive (false);
			m_reverseTimeLeft += m_ReverseTime;		//add reverse time
			tempObject = other.gameObject;
			audioSource.clip = Reverse;				//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitTwentyFiveSeconds");		//calling of method
		} else if (other.gameObject.tag == "ItemFrozen") {
			other.gameObject.SetActive (false);
			m_frozenTimeLeft += m_FrozenTime;		//add frozen time
			tempObject = other.gameObject;
			audioSource.clip = Frozen;				//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitFifteenSeconds");			//calling of method
		} else if (other.gameObject.tag == "Gas") {
			//m_CoinsCollected [m_NumCoinsCollected] = other.gameObject;
			m_GasCollected[m_NumGasCollected] = other.gameObject;
			other.gameObject.SetActive (false);
			//m_NumCoinsCollected++;				//add 1 to number of coins
			m_NumGasCollected ++;
			audioSource.clip = GetCoin;			//play sound effect
			audioSource.Play ();
			//BlueCoinText.text = ("Coins: " + m_NumCoinsCollected);
			BlueGasText.text = ("Gas: " + (m_NumGasCollected + m_NumExtraGas));
		} else if (other.gameObject.tag == "ItemDeductGas") {
			_ScriptGreenCarMovement.deductCoins ();		//calling of deductCoins method
			other.gameObject.SetActive (false);
			tempObject = other.gameObject;
			audioSource.clip = DropCoin;			//play sound effect
			audioSource.Play ();
			StartCoroutine ("waitNinetySeconds");		//calling of method
		} else if (other.gameObject.tag == "ItemHideAndSeek") {
			for (byte i = 0; i < 8; i++) {
				ItemHideAndSeek [i].SetActive (false);
			}
			m_HideAndSeekTimeLeft += 60f;
			m_HideAndSeekMode = true;
			timeticking.Play ();
			StartCoroutine ("waitTenSeconds");
		} else if (other.gameObject == opponent) {
			if (m_HideAndSeekMode == true) {
				textGreenHide.SetActive (false);
				opponent.GetComponent<GreenCarMovement> ().TextBlueHide.SetActive (false);
				timeticking.Pause ();
				_ScriptTimeInGame.m_HideAndSeekMode= true;
				_ScriptTimeInGame.GreenOrBlueWins = 1;
				_ScriptTimeInGame.wholeGameCountingDownTime = -1;
			}
		}
	}

	IEnumerator waitTwentyFiveSeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (m_ReverseTime + 2f);
		m_LocalTemp.SetActive (true);			//activate item after 27s
	}

	IEnumerator waitFifteenSeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (30f);
		m_LocalTemp.SetActive (true);			//activate item after 30s
	}

	IEnumerator waitNinetySeconds(){
		GameObject m_LocalTemp = tempObject;
		yield return new WaitForSeconds (60);
		m_LocalTemp.SetActive (true);			//activate item after 60s
	}

	IEnumerator waitTenSeconds(){
		yield return new WaitForSeconds (60);	//activate item after 10s
		for (byte i = 0; i < 8; i++) {
			ItemHideAndSeek [i].SetActive (true);
		}
	}

	public void deductCoins(){

		/*
		if (m_NumCoinsCollected > 0) {		//if coins are collected
			m_CoinsCollected [0].SetActive (true);		//activate the first coin collcted
			m_NumCoinsCollected--;			//decrease coin
			for (byte i = 0; i < m_NumCoinsCollected; i++) {
				m_CoinsCollected [i] = m_CoinsCollected [i + 1];		//switch value
			}
			BlueCoinText.text = ("Coins: " + m_NumCoinsCollected);		//output new value
		}*/

		if (m_NumGasCollected > 0) {
			m_GasCollected [0].SetActive (true);
			m_NumGasCollected--;
			for (byte i = 0; i < m_NumGasCollected; i++) {
				m_GasCollected [i] = m_GasCollected [i + 1];
			}
			BlueGasText.text = ("Gas: " + (m_NumGasCollected + m_NumExtraGas));
		} else {
			if (m_NumExtraGas > 1) {
				m_NumExtraGas--;
				BlueGasText.text = ("Gas: " + m_NumExtraGas);
			}else{
				textGreenHide.SetActive (false);
				opponent.GetComponent<GreenCarMovement> ().TextBlueHide.SetActive (false);
				opponent.GetComponent<GreenCarMovement> ().timeticking.Pause ();
				m_NumExtraGas--;
				BlueGasText.text = "Gas: " + m_NumExtraGas;
				_ScriptTimeInGame.wholeGameCountingDownTime = -1;
			}
		}
	}
}
