﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenIceCube : MonoBehaviour {

	public Animator anim;				//Declaring variables and objects
	public bool animPlay;

	private bool animPlaying;
	private bool animEnding;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();		//initializing variable
		animPlaying = false;
	}

	public void playVideo(){
		if (animPlaying == false) {
			anim.Play ("Take 001");		//play video
			animPlaying = true;
		}
	}

	public void endVideo(){
		if (animPlaying == true){
			anim.Play("Take 002");		//play second video
			animPlaying = false;
		}
	}

}
