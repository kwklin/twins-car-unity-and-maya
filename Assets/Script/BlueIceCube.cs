﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueIceCube : MonoBehaviour {

	public Animator anim;			//Declaring variable and objects
	public bool animPlay;

	private bool animPlaying;
	private bool animEnding;

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		animPlaying = false;			//initialize variable
	}

	public void playVideo(){
		if (animPlaying == false) {
			anim.Play ("Take 001");		//play animation
			animPlaying = true;
		}
	}

	public void endVideo(){
		if (animPlaying == true){
			anim.Play("Take 002");		//play second animation
			animPlaying = false;
		}
	}

}
