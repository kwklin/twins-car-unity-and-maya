﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}

	public void loadOtherScene(string levelName){
		Time.timeScale = 1;			//set time scale before calling other scenes
		Application.LoadLevel (levelName);
	}

	public void quitProgram(){
		Application.Quit();			//quit program
	}
}
