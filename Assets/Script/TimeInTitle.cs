﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeInTitle : MonoBehaviour {

	public Animator anim;				//declaring variables and objects
	public GameObject loadingPanel;
	public GameObject startText;

	// Use this for initialization
	void Start () {
		Time.timeScale = 4;				//faster animation speed
		StartCoroutine ("waitTwoSecond");		//calling of method
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.anyKeyDown) {			//if any key is pressed
			startText.SetActive (false);		
			anim.Play ("Take 002");		//play animation
			StartCoroutine ("waitTwoSeconds");
		}
	}

	IEnumerator waitTwoSecond(){
		yield return new WaitForSecondsRealtime (2f);
		startText.SetActive (true);		//activate text after 2 seconds
	}

	IEnumerator waitTwoSeconds(){
		yield return new WaitForSeconds (3f);
		Time.timeScale = 1;				//real time scale
		loadingPanel.SetActive (true);	//activate panel after 3 seconds
		Application.LoadLevel ("Main Menu");
	}
}
