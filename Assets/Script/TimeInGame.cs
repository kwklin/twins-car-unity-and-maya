﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimeInGame : MonoBehaviour {
	
	public Text countingDownText;				//Declaring objects and variables
	public Text wholeGameCountingDownText;
	public Text countingLastTenText;
	public GameObject greenCar;
	public GameObject blueCar;
	public GameObject endPanel;
	public Text textCount1;
	public Text textCount2;
	public Text Result1;
	public Text Result2;
	public GameObject instruction;
	public float wholeGameCountingDownTime;
	public bool m_HideAndSeekMode;
	public byte GreenOrBlueWins;	//0 or 1
	public GameObject GreenWins;
	public GameObject BlueWins;
	public GameObject bigMap;
	public GameObject textToBeRemoved;
	public GameObject textToBeRemoved2;
	public GameObject buttonPause;
	public GameObject text;

	private float countingDownTime;
	private bool mapShowing;
	private bool firstEntered;

	// Use this for initialization
	void Start () {
		countingDownTime = 4f;				//initializing variables
		wholeGameCountingDownTime = 300f;
		m_HideAndSeekMode = false;
		mapShowing = false;
		buttonPause.SetActive (false);
		firstEntered = true;
	}

	// Update is called once per frame
	void Update () {
		if (countingDownTime > 0) {		//counting from 3 to 0
			wholeGameCountingDownText.gameObject.SetActive (false);			//disactivate text
			countingLastTenText.gameObject.SetActive (false);				//disactivate text
			if (Mathf.Round(countingDownTime) == 4) {
				countingDownTime -= Time.deltaTime;
				countingDownText.gameObject.SetActive (false);
			} else if(Mathf.Round(countingDownTime) == 0){
				countingDownTime -= Time.deltaTime;
				countingDownText.text = "Go!";		//if it's the last second then output go
			}else {
				countingDownTime -= Time.deltaTime;		//if it's not 0 and 4
				countingDownText.gameObject.SetActive (true);
				countingDownText.text = "" + Mathf.Round (countingDownTime);		//output count down value
			}
			greenCar.GetComponent<GreenCarMovement> ().enabled = false;			//disable scripts
			blueCar.GetComponent<BlueCarMovement> ().enabled = false;
		} else {
			if (firstEntered) {
				text.SetActive (true);
				firstEntered = false;
			}
			buttonPause.SetActive (true);
			if(Input.GetKeyDown(KeyCode.Space)){
				if (!mapShowing) {
					bigMap.SetActive (true);
					mapShowing = true;
					text.SetActive (false);
				} else {
					bigMap.SetActive (false);
					mapShowing = false;
					text.SetActive (true);
				}
			}
			instruction.SetActive (false);		//disactivate instruction panel
			countingDownTime = 0;
			countingDownText.gameObject.SetActive (false);
			greenCar.GetComponent<GreenCarMovement> ().enabled = true;			//enable scripts
			blueCar.GetComponent<BlueCarMovement> ().enabled = true;
			wholeGameCountingDownText.gameObject.SetActive (true);		//activate counting text
			wholeGameCountingDownTime -= Time.deltaTime;				//countdown
			if (Mathf.Round (wholeGameCountingDownTime % 60) == 60) {			//if second is 00
				wholeGameCountingDownText.text = Mathf.Round ((wholeGameCountingDownTime)/60) + ":00";
			} else {
				if (wholeGameCountingDownTime % 60 >= 10) {		//if more than ten seconds in second component left
					wholeGameCountingDownText.text = (int)Mathf.Round (wholeGameCountingDownTime) / 60 + ":" + Mathf.Round (wholeGameCountingDownTime % 60);
				} else {										//if less than ten seconds in second component left
					wholeGameCountingDownText.text = (int)Mathf.Round (wholeGameCountingDownTime) / 60 + ":0" + Mathf.Round (wholeGameCountingDownTime % 60);
				}
			}
			if (Mathf.Round(wholeGameCountingDownTime) <= 10) {			//if less than 10 seconds left;
				wholeGameCountingDownText.gameObject.SetActive (false);		//disactivate text
				countingLastTenText.gameObject.SetActive (true);			//activate text
				countingLastTenText.text = "" + Mathf.Round (wholeGameCountingDownTime);		//output time
				if (Mathf.Round (wholeGameCountingDownTime) < 0 && m_HideAndSeekMode == false) {					//if there's no time left
					text.SetActive(false);
					countingLastTenText.gameObject.SetActive (false);				//disactivate text
					greenCar.GetComponent<GreenCarMovement> ().enabled = false;		//disable scripts
					blueCar.GetComponent<BlueCarMovement> ().enabled = false;
					endPanel.SetActive (true);			//activate panel
					textCount1.text = "" + (int)(greenCar.GetComponent<GreenCarMovement> ().m_NumGasCollected + greenCar.GetComponent<GreenCarMovement> ().m_NumExtraGas);		//get number of coins collected
					textCount2.text = "" + (int)(blueCar.GetComponent<BlueCarMovement> ().m_NumGasCollected + blueCar.GetComponent<BlueCarMovement> ().m_NumExtraGas);
					if ((greenCar.GetComponent<GreenCarMovement> ().m_NumGasCollected + greenCar.GetComponent<GreenCarMovement> ().m_NumExtraGas) > (blueCar.GetComponent<BlueCarMovement> ().m_NumGasCollected + blueCar.GetComponent<BlueCarMovement> ().m_NumExtraGas)) {
						Result1.text = "You Win";		//output results
						Result2.text = "You Lose";
					} else if ((blueCar.GetComponent<BlueCarMovement> ().m_NumGasCollected + blueCar.GetComponent<BlueCarMovement> ().m_NumExtraGas) > (greenCar.GetComponent<GreenCarMovement> ().m_NumGasCollected + greenCar.GetComponent<GreenCarMovement> ().m_NumExtraGas)) {			//+...
						Result1.text = "You Lose";		//output results
						Result2.text = "You Win";
					} else {
						Result1.text = "Tie";			//output results
						Result2.text = "Tie";
					}
				} else if (Mathf.Round (wholeGameCountingDownTime) < 0 && m_HideAndSeekMode == true) {
					text.SetActive (false);
					textToBeRemoved.SetActive (false);
					textToBeRemoved2.SetActive (false);
					countingLastTenText.gameObject.SetActive (false);				//disactivate text
					greenCar.GetComponent<GreenCarMovement> ().enabled = false;		//disable scripts
					blueCar.GetComponent<BlueCarMovement> ().enabled = false;
					if (GreenOrBlueWins == 0) {
						GreenWins.SetActive (true);
					} else {
						BlueWins.SetActive (true);
					}
				}
			}
		}

	}

}
