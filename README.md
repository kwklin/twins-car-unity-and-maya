# Twins Car - Unity and Maya
This is a hide and seek game where objects were created using Maya and the game was built using Unity.

## Goal
There are two ways to win before the game ends. The first way is to remove all of opponent's gases, and the second way is to catch the opponent when the hide and seek-mode is on. Each game lasts 5 minutes. If none of the players win

## Game Control
![Control](Images/Buttons.png "Control")
Players can control the cars using the a, w, s, d keys, and the arrow keys.

## Rules
This is a game of two players. Each player starts with 2 gases. Each game lasts 5 minutes if none of the players win. Once the game starts, you will see a map, a car, the number of gases on each of the player's side, and a time in the middle.
![Game](Images/Game.png "Game")
During the game, there are items to be collected, including Gas, Hide and Seek, Reverse Action, Frozen and Deduct Gas. These items have time limits, but would help you win the game.

### Gas
![Gas](Images/Gas.png "Gas")
Gases are needed for a car to move. If you run out of gases, you will lose. Everytime you collect a gas, it will be added to your car. 

### Deduct Gas
![Deduct_Gas](Images/Deduct_Gas.png "Deduct Gas")
By collecting this type of items, you will remove one gas from your opponent.

### Reverse Action
![Reverse_Action](Images/Reverse_Action.png "Reverse Action")
This item will help you by reversing you opponent's actions. For example, you opponent's forward action will become a backward action, and left will become right.

### Frozen
![Frozen](Images/Frozen.png "Frozen")
By freezing your opponent, you will be able to have a higher chance to hide or catch your opponent.

### Hide and Seek
![Hide_and_Seek](Images/Hide_and_Seek.png "Hide and Seek")
Collecting this item will turn on the Hide and Seek mode. If you turns on the hide and seek mode, catch your opponent and your opponent will lose immediately. 
![Hide_and_Seek_Mode_On](Images/Hide_and_Seek_Countdown.png "Hide and Seek Mode On")
If your opponent turns on the hide and seek mode, hide from your opponent and avoid getting caught.

## Game Setup
Unfortunately, this is actually my high school project which I didn't keep the codes because I didn't think that was important. 
This repository contains the all files needed to run the game. You can do this by cloning this whole repository and run "Twins Car.exe".